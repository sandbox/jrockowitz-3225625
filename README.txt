### About this Module

The Webform Confirmation adds the ability to assign update submission access to
a user by user id or role.

### Notes

This module provides an MVP solution that allows users to assign update access
to a submission. The concept is a form builder would create a user entity
reference element beginning with 'assign_to' and the selected user ids or roles
would be allowed to update the submission.

This approach allows form builders to have complete control over the 'assign_to'
element. For example, a form builder could limit the 'assign_to' user entity
reference element to a role or create a simple select menu with user id's as
the option values.

IMPORTANT!!!
Currently, this module DOES NOT support Views integration.

To support Views integration, we would also have to add a
`hook_webform_submission_query_access_alter()` hook that would apply the
'apply_to' rules to any submission-related query.
