<?php

namespace Drupal\Tests\webform_assign_to\Functional;

use Drupal\Tests\webform\Traits\WebformSubmissionViewAccessTrait;
use Drupal\Tests\webform_group\Functional\WebformGroupBrowserTestBase;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Tests for webform group access submission views.
 *
 * @group webform_group
 */
class WebformAssignToGroupTest extends WebformGroupBrowserTestBase {

  use WebformSubmissionViewAccessTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['webform_assign_to', 'webform_group', 'webform_group_test', 'views', 'webform_test_views'];

  /**
   * Tests webform assign to group access.
   *
   * @see \Drupal\Tests\webform_group\Functional\WebformGroupSubmissionAccessTest
   */
  public function testWebformAssignToGroupTest() {
    $assert_session = $this->assertSession();

    // Webform.
    $webform = Webform::load('example_assign_to_group');

    // Default group.
    $group = $this->createGroup(['type' => 'default']);

    // Webform node.
    $node = $this->createWebformNode('example_assign_to_group');
    $nid = $node->id();

    // Add webform node to group.
    $group->addContent($node, 'group_node:webform');
    $group->save();

    // Create member.
    $member = $this->createUser();
    $group->addMember($member);

    // Create custom user and role..
    $custom = $this->createUser();
    $group->addMember($custom, ['group_roles' => ['default-custom']]);
    $group->save();

    /** @var \Drupal\webform\WebformSubmissionGenerateInterface $submission_generate */
    $submission_generate = \Drupal::service('webform_submission.generate');

    /** @var \Drupal\webform\WebformSubmissionInterface $submission */
    $submission = WebformSubmission::create([
      'webform_id' => 'example_assign_to_group',
      'entity_type' => 'node',
      'entity_id' => $node->id(),
      // Must set default data for all assign to elements to prevent submission
      // generator from populating these values.
      'data' => [
        'assign_to_group' => [],
        'assign_to_webform_group_role' => [],
      ] + $submission_generate->getData($webform),
    ]);
    $submission->save();
    $sid = $submission->id();

    /**************************************************************************/
    // Group role.
    /**************************************************************************/

    // Confirm that anonymous is denied access to the submission.
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(403);

    // Confirm that member is denied access to the submission.
    $this->drupalLogin($member);
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(403);

    // Confirm that custom is denied access to the submission.
    $this->drupalLogin($custom);
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(403);

    // Allow member access to the submission.
    $submission
      ->setElementData('assign_to_group', [])
      ->setElementData('assign_to_webform_group_role', ['member'])
      ->save();

    // Confirm that member is allowed access to the submission.
    $this->drupalLogin($member);
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(200);

    // Confirm that custom is allowed access to the submission.
    $this->drupalLogin($custom);
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(200);

    // Allow custom  access to the submission.
    $submission
      ->setElementData('assign_to_group', [])
      ->setElementData('assign_to_webform_group_role', ['default-custom'])
      ->save();

    // Confirm that member is denied access to the submission.
    $this->drupalLogin($member);
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(403);

    // Confirm that custom is allowed access to the submission.
    $this->drupalLogin($custom);
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(200);

    /**************************************************************************/
    // Group role.
    /**************************************************************************/

    $submission
      ->setElementData('assign_to_group', [])
      ->setElementData('assign_to_webform_group_role', [])
      ->save();

    // Confirm that anonymous is denied access to the submission.
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(403);

    // Confirm that member is denied access to the submission.
    $this->drupalLogin($member);
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(403);

    // Confirm that custom is denied access to the submission.
    $this->drupalLogin($custom);
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(403);

    // Allow group access to the submission.
    $submission
      ->setElementData('assign_to_group', [$group->id()])
      ->setElementData('assign_to_webform_group_role', [])
      ->save();

    // Confirm that member is allowed access to the submission.
    $this->drupalLogin($member);
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(200);

    // Confirm that custom is allowed access to the submission.
    $this->drupalLogin($custom);
    $this->drupalGet("/node/$nid/webform/submission/$sid");
    $assert_session->statusCodeEquals(200);
  }

}
