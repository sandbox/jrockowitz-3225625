<?php

namespace Drupal\Tests\webform_assign_to\Functional;

use Drupal\Tests\webform\Functional\WebformBrowserTestBase;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Webform Assign To browser test.
 *
 * @group webform_browser
 */
class WebformAssignToTest extends WebformBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['webform', 'webform_assign_to'];

  /**
   * Test assign to element.
   */
  public function testAssignToElement() {
    $assert_session = $this->assertSession();

    // Load webform.
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = Webform::load('example_assign_to');

    // Create reviewer without edit access.
    $reviewer = $this->createUser(['access webform overview']);

    // Allow reviewer to view the webform's submissions.
    $access_rules = $webform->getAccessRules();
    $access_rules['view_any']['users'] = [$reviewer->id()];
    $webform->setAccessRules($access_rules);
    $webform->save();

    /* ********************************************************************** */

    // Login as admin.
    $this->drupalLogin($this->rootUser);

    // Create a test submission.
    $sid = $this->postSubmissionTest($webform);
    /** @var \Drupal\webform\WebformSubmissionInterface $submission */
    $submission = WebformSubmission::load($sid);
    // Must set default data for all assign to elements to prevent submission
    // generator from populating these values.
    $submission
      ->setElementData('assign_to_user', [])
      ->setElementData('assign_to_role', [])
      ->setElementData('assign_to_user_self_access', [])
      ->save();

    // Logout admin.
    $this->drupalLogout();

    // Login as reviewer.
    $this->drupalLogin($reviewer);

    // Confirm reviewer user can view the webform.
    $this->drupalGet('/admin/structure/webform');
    $assert_session->statusCodeEquals(200);
    $this->assertSession()->linkExists('Example: Assign to');

    // Confirm reviewer user can view the webform's submissions.
    $this->drupalGet('/admin/structure/webform/manage/example_assign_to/results/submissions');
    $assert_session->statusCodeEquals(200);
    $this->assertSession()->linkExists($sid);
    $this->assertSession()->linkExists('View');

    // Confirm reviewer user can view the webform's submissions.
    $this->drupalGet("/admin/structure/webform/manage/example_assign_to/submission/$sid");
    $assert_session->statusCodeEquals(200);

    // Confirm reviewer user can NOT update the webform's submissions.
    $this->drupalGet("/admin/structure/webform/manage/example_assign_to/submission/$sid/edit");
    $assert_session->statusCodeEquals(403);

    /* ********************************************************************** */

    // Assign user id to test submission.
    $submission
      ->setElementData('assign_to_user', [$reviewer->id()])
      ->setElementData('assign_to_role', [])
      ->setElementData('assign_to_user_self_access', [])
      ->save();

    // Confirm reviewer can access views submission.
    $this->drupalGet("/admin/structure/webform/manage/example_assign_to/submission/$sid");
    $assert_session->statusCodeEquals(200);

    // Confirm reviewer user can now update the submission.
    $this->drupalGet("/admin/structure/webform/manage/example_assign_to/submission/$sid/edit");
    $assert_session->statusCodeEquals(200);

    /* ********************************************************************** */

    // Assign user id to test submission.
    $submission
      ->setElementData('assign_to_user', [])
      ->setElementData('assign_to_role', [])
      ->setElementData('assign_to_user_self_access', [])
      ->save();

    // Confirm reviewer user can NOT update the webform's submissions.
    $this->drupalGet("/admin/structure/webform/manage/example_assign_to/submission/$sid/edit");
    $assert_session->statusCodeEquals(403);

    /* ********************************************************************** */

    // Assign user role to test submission.
    $submission
      ->setElementData('assign_to_user', [])
      ->setElementData('assign_to_role', $reviewer->getRoles(TRUE))
      ->setElementData('assign_to_user_self_access', [])
      ->save();

    // Confirm reviewer can access views submission.
    $this->drupalGet("/admin/structure/webform/manage/example_assign_to/submission/$sid");
    $assert_session->statusCodeEquals(200);

    // Confirm reviewer user can now update the submission.
    $this->drupalGet("/admin/structure/webform/manage/example_assign_to/submission/$sid/edit");
    $assert_session->statusCodeEquals(200);

    /* ********************************************************************** */

    $submission
      ->setElementData('assign_to_user', [$reviewer->id()])
      ->setElementData('assign_to_role', [])
      ->setElementData('assign_to_user_self_access', [])
      ->save();

    // Confirm self assign element is visible when not used to grant access.
    $this->drupalGet("/admin/structure/webform/manage/example_assign_to/submission/$sid/edit");
    $this->assertFieldByName('assign_to_user_self_access[]');

    $submission
      ->setElementData('assign_to_user', [])
      ->setElementData('assign_to_role', [])
      ->setElementData('assign_to_user_self_access', [$reviewer->id()])
      ->save();

    // Confirm reviewer can access views submission.
    $this->drupalGet("/admin/structure/webform/manage/example_assign_to/submission/$sid");
    $assert_session->statusCodeEquals(200);

    // Confirm self assign element is NOT visible when used to grant access.
    $this->drupalGet("/admin/structure/webform/manage/example_assign_to/submission/$sid/edit");
    $assert_session->statusCodeEquals(200);
    $this->assertNoFieldByName('assign_to_user_self_access[]');
  }

}
