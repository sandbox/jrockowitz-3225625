<?php

namespace Drupal\webform_assign_to\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_group\Element\WebformGroupRoles as WebformGroupRolesElement;
use Drupal\webform\Plugin\WebformElement\Select;

/**
 * Provides a 'Webform Group Roles' element.
 *
 * @WebformElement(
 *   id = "webform_group_roles",
 *   label = @Translation("Webform Group Roles"),
 *   description = @Translation("Provides a form element for selecting webform group roles."),
 *   category = @Translation("Custom elements"),
 *   dependencies = {
 *     "webform_group",
 *   },
 * )
 */
class WebformGroupRoles extends Select {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    $properties = [
      'include_internal' => TRUE,
      'include_user_roles' => FALSE,
      'include_anonymous' => FALSE,
      'include_outsider' => TRUE,
    ] + parent::defineDefaultProperties();
    // Remove options related properties.
    unset(
      $properties['options'],
      $properties['options_randomize'],
      $properties['empty_option'],
      $properties['empty_value'],
      $properties['sort_options']
    );
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function initialize(array &$element) {
    parent::initialize($element);
    $element['#options'] = WebformGroupRolesElement::getGroupRolesOptions($element);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['group_roles'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Group roles settings'),
      '#open' => TRUE,
    ];
    $form['group_roles']['include_internal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include internal roles'),
      '#return_value' => TRUE,
    ];
    $form['group_roles']['include_user_roles'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include user roles'),
      '#return_value' => TRUE,
    ];
    $form['group_roles']['include_anonymous'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include anonymous roles'),
      '#return_value' => TRUE,
    ];
    $form['group_roles']['include_outsider'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include outsider roles'),
      '#return_value' => TRUE,
    ];
    return $form;
  }

}
